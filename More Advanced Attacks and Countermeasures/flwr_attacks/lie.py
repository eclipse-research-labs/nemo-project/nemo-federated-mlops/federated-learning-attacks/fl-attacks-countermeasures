#-------------------------------------------------------------------------------
# Copyright 2024  Sphynx Technology Solutions and Aristotle University of Thessaloniki
# 
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy
# of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
# 
# SPDX-License-Identifier: Apache-2.0
#-------------------------------------------------------------------------------
from typing import Dict, List, Tuple, Union
from flwr_attacks.model_poisoning import ModelPoisoningAttack
from flwr.server.client_proxy import ClientProxy
from flwr.common import (
	FitRes,
	Parameters,
	Scalar,
)

from flwr.common import NDArrays

from flwr.common.parameter import (
	ndarrays_to_parameters,
)
from flwr_attacks.algorithms import lie_attack


class LieAttack(ModelPoisoningAttack):
	def __init__(
		self,
		*,
		adversary_fraction: int = 0.2,
		adversary_accessed_cids: List[str] = None,
		adversary_clients: List[str] = None,
		activation_round: int = 0,
		total_clients: int,

		**kwargs,
	) -> None:
		super().__init__(
			adversary_fraction=adversary_fraction,
			adversary_accessed_cids=adversary_accessed_cids,
			adversary_clients=adversary_clients,
			activation_round=activation_round,
			**kwargs,
		)
		self.total_clients = total_clients

	def attack_fit(
        self,
        server_round: int,
        results: List[Tuple[ClientProxy, FitRes]],
        failures: List[Union[Tuple[ClientProxy, FitRes], BaseException]],
        parameters: Parameters,
	) -> Tuple[
        List[Tuple[ClientProxy, FitRes]],
        List[Union[Tuple[ClientProxy, FitRes], BaseException]],
        Dict[str, Scalar],
    ]:

		metrics = {}

		adversial_parameters: List[Tuple[NDArrays, bool]] = self.extract_adversaries(
			results
		)
		if len(adversial_parameters) == 0:
			return results, failures, metrics

		if server_round > self.activation_round:
			malicious_gradient = lie_attack(
				[params for params, _ in adversial_parameters],
				total_clients=self.total_clients,
				total_malicious=int(self.total_clients*self.adversary_fraction),
			)
			
			for client, fit_res in results:
				if client.cid in self.adversary_clients:
					fit_res.parameters = ndarrays_to_parameters(malicious_gradient)
		
		return results, failures, metrics
					
			
