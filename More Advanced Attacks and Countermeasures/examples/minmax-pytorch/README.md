<!---
 Copyright 2024  Sphynx Technology Solutions and Aristotle University of Thessaloniki
 
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License.  You may obtain a copy
 of the License at
 
   http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 License for the specific language governing permissions and limitations under
 the License.
 
 SPDX-License-Identifier: Apache-2.0
-->

# Applying the MinMax Attack on Flower Simulation Using PyTorch

This example is an extended version of the example from the adap/flwr project https://github.com/adap/flower/blob/main/examples/simulation-pytorch/sim.ipynb. The original example was created by the authors of the adap/flwr project.


## Running on Google Colab

Run the example on Google Colab: [![Open in Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/n45os/flwr_attacks/blob/main/examples/minmax-pytorch/sim.ipynb)
